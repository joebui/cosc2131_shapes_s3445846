#ifndef WEEK2_APP1_RECTANGLE_H
#define WEEK2_APP1_RECTANGLE_H

#include "Shape.h"
#include "Line.h"

namespace program {
    class Rectangle : public Shape {
    private:
        Point p1;
        Point p2;
        Point p3;
        Point p4;

    public:
        Rectangle(Point p1, Point p2, Point p3, Point p4): p1{p1}, p2{p2}, p3{p3}, p4{p4} {}

        // Access the points of the rectangle.
        Point operator[] (int x) {
            if (x == 1) {
                return p1;
            } else if (x == 2) {
                return p2;
            } else if (x == 3) {
                return p3;
            } else {
                return p4;
            }
        }

        // Move the rectangle to different location.
        virtual void move(int xx, int yy) {
            p1.move(xx, yy);
            p2.move(xx, yy);
            p3.move(xx, yy);
            p4.move(xx, yy);
        }

        // Set and display the shape on the screen.
        virtual void draw(Screen &screen) {
            Line width1 {p1, p2};
            Line height1 {p2, p3};
            Line width2 {p3, p4};
            Line height2 {p4, p1};

            width1.draw(screen);
            height1.draw(screen);
            width2.draw(screen);
            height2.draw(screen);
        }
    };
}

#endif //WEEK2_APP1_RECTANGLE_H
