#ifndef WEEK2_APP1_POLYGON_H
#define WEEK2_APP1_POLYGON_H

#include "Line.h"

namespace program {
    class Polygon : public Shape {
    private:
        Point p1;
        Point p2;
        Point p3;
        Point p4;
        Point p5;

    public:
        Polygon(Point p1, Point p2, Point p3, Point p4, Point p5): p1{p1}, p2{p2}, p3{p3}, p4{p4}, p5{p5} {}

        // Access the points of the polygon.
        Point operator[] (int x) {
            if (x == 1) {
                return p1;
            } else if (x == 2) {
                return p2;
            } else if (x == 3) {
                return p3;
            } else if (x == 4) {
                return p4;
            } else {
                return p5;
            }
        }


        // Move the polygon to different location.
        virtual void move(int xx, int yy) {
            p1.move(xx, yy);
            p2.move(xx, yy);
            p3.move(xx, yy);
            p4.move(xx, yy);
            p5.move(xx, yy);
        }

        // Set and display the shape on the screen.
        virtual void draw(Screen &screen) {
            Line line1 {p1, p2};
            Line line2 {p2, p3};
            Line line3 {p3, p4};
            Line line4 {p4, p5};
            Line line5 {p5, p1};

            // Set the symbol for the lines.
            line1.setDraw_char('&');
            line2.setDraw_char('&');
            line3.setDraw_char('&');
            line4.setDraw_char('&');
            line5.setDraw_char('&');

            line1.draw(screen);
            line2.draw(screen);
            line3.draw(screen);
            line4.draw(screen);
            line5.draw(screen);
        }
    };
}

#endif //WEEK2_APP1_POLYGON_H
