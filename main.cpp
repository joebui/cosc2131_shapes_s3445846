#include <iostream>
#include "Point.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Polyline.h"
#include "Polygon.h"
#include "Image.h"

using namespace std;
using namespace program;

int main() {
    // Initialize the screen.
    Screen screen{20, 15};

    // Create rectangle shape.
    program::Point r1 {0, 0};    program::Point r2 {3, 0};
    program::Point r3 {3, 5};    program::Point r4 {0, 5};
    program::Shape *r = new program::Rectangle(r1, r2, r3, r4);

    // Create circle shape.
    program::Point center {7, 2};
    program::Shape *c = new program::Circle (center, 2);

    // Create polyline shape.
    program::Point pl1 {0, 7};    program::Point pl2 {3, 10};
    program::Point pl3 {6, 10};    program::Point pl4 {8, 8};
    program::Point pl5 {12, 12};
    program::Shape *pl = new program::Polyline (pl1, pl2, pl3, pl4, pl5);

    // Create polygon shape.
    program::Point pg1 {15, 2};    program::Point pg2 {13, 4};
    program::Point pg3 {13, 8};    program::Point pg4 {17, 8};
    program::Point pg5 {17, 4};
    program::Shape *pg = new program::Polygon (pg1, pg2, pg3, pg4, pg5);

    // Print the shapes on the screen,
    Image image {r, c, pl, pg};
    image.draw(screen);

    // Export the shapes to a svg file.
    cout << "Output to svg image file: svg_shapes.svg" << endl << endl;
    image.outputToSVGFile(dynamic_cast<program::Rectangle*>(r), dynamic_cast<program::Circle*>(c),
                          dynamic_cast<program::Polyline*>(pl), dynamic_cast<program::Polygon*>(pg));

    // Export the shapes to a png file.
    cout << "Output to png image file: png_shapes.png" << endl << endl;
    image.outputToPNGFile(screen);

    // Export the shapes to a text file.
    cout << "Write to text file: shapes.txt" << endl << endl;
    image.writeToTextFile(screen, dynamic_cast<program::Rectangle*>(r), dynamic_cast<program::Circle*>(c),
                          dynamic_cast<program::Polyline*>(pl), dynamic_cast<program::Polygon*>(pg));

    // Import the shapes from text file.
    cout << "Read again from text file" << endl << endl;
    image.readFromTextFile();
    image.draw(screen);

    // Create a new rectangle and display it to screen.
    program::Point r11 {0, 14};    program::Point r12 {0, 12};
    program::Point r13 {3, 12};    program::Point r14 {3, 14};
    program::Shape *newR = new program::Rectangle(r11, r12, r13, r14);
    cout << "Add new Shape to screen" << endl << endl;
    image.addShape(newR);
    image.draw(screen);

    // Remove the 1st rectangle.
    cout << "Remove 1st shape" << endl << endl;
    image.removeShape(0, screen);
    image.draw(screen);

    // Move the circle to right.
    cout << "Move the circle" << endl << endl;
    screen.cleanScreen();
    image.getShapes().at(0)->move(2, 2);
    image.draw(screen);

    return 0;
}