#ifndef WEEK2_APP1_SHAPE_H
#define WEEK2_APP1_SHAPE_H

#include "Screen.h"

namespace program {
    class Shape {
    public:
        virtual void move(int xx, int yy) = 0;
        virtual void draw(Screen &screen) = 0;

        char getDraw_char() const {
            return draw_char;
        }

        void setDraw_char(char draw_char) {
            Shape::draw_char = draw_char;
        }

    protected:
        char draw_char = '*';
    };
}

#endif //WEEK2_APP1_SHAPE_H