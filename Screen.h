//
// Created by bit on 10/26/15.
//

#ifndef WEEK2_APP1_SCREEN_H
#define WEEK2_APP1_SCREEN_H

using namespace std;

class Screen {
public:
    Screen(int w, int h): width{w}, height{h}, length{width * height} {
        screen = new char[length];
        for (int i = 0; i < length; ++i) {
            screen[i] = ' ';
        }
    }

    int getWidth() const {
        return width;
    }

    void setWidth(int width) {
        Screen::width = width;
    }

    int getHeight() const {
        return height;
    }

    // Print the shapes on screen.
    friend ostream& operator<<(ostream& out, Screen scr) {
        for (int i = 0; i < scr.height; ++i) {
            for (int j = 0; j < scr.width; ++j) {
                cout << scr.getChar(j, i) << ' ';
            }
            cout << endl;
        }
        return out;
    }

    void setChar(int x, int y, char c) {
        screen[width * y + x] = c;
    }

    char getChar(int x, int y) const {
        return screen[width * y + x];
    }

    void cleanScreen() {
        for (int i = 0; i < length; ++i) {
            screen[i] = ' ';
        }
    }
private:
    int width;
    int height;
    int length;
    char *screen;
};


#endif //WEEK2_APP1_SCREEN_H
