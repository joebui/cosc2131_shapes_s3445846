// Image class should not be subclass of Shape.

#ifndef WEEK2_APP1_IMAGE_H
#define WEEK2_APP1_IMAGE_H

#include <vector>
#include <iostream>
#include <fstream>
#include "simple_svg_1.0.0.hpp"
#include "Shape.h"
#include <png++/png.hpp>

using namespace svg;
using namespace std;
using namespace program;

class Image {
private:
    vector<program::Shape*> shapes;

public:
    Image(program::Shape *s1, program::Shape *s2, program::Shape *s3, program::Shape *s4): shapes {
            s1, s2, s3, s4
    } {}

    const vector<program::Shape *> &getShapes() const {
        return shapes;
    }

    void draw(Screen &screen) {
        for (unsigned long i = 0; i < shapes.size(); ++i) {
            shapes.at(i)->draw(screen);
        }

        cout << screen << endl;
    }

    void addShape(program::Shape *newShape) {
        shapes.push_back(newShape);
    }

    void removeShape(int position, Screen &screen) {
        shapes.erase(shapes.begin() + position);
        screen.cleanScreen();
    }

    void writeToTextFile(Screen &screen, program::Rectangle *r, program::Circle *cr, program::Polyline *pl,
                         program::Polygon *pg) {
        filebuf shapeFile;
        shapeFile.open("shapes.txt", ios::out);
        ostream os (&shapeFile);

        // Output the positions of the points of the shapes.
        os << r->operator[](1)[0] << ',' << r->operator[](1)[1] << ';' << r->operator[](2)[0] << ',' << r->operator[](2)[1] << ';'
        << r->operator[](3)[0] << ',' << r->operator[](3)[1] << ';' << r->operator[](4)[0] << ',' << r->operator[](4)[1] << endl;
        os << cr->operator[](1)[0] << ',' << cr->operator[](1)[1] << ';' << cr->getRadius() << ',' << '0' << endl;
        os << pl->operator[](1)[0] << ',' << pl->operator[](1)[1] << ';' << pl->operator[](2)[0] << ',' << pl->operator[](2)[1] << ';'
        << pl->operator[](3)[0] << ',' << pl->operator[](3)[1] << ';' << pl->operator[](4)[0] << ',' << pl->operator[](4)[1] <<
        ';' << pl->operator[](5)[0] << ',' << pl->operator[](5)[1] << endl;
        os << pg->operator[](1)[0] << ',' << pg->operator[](1)[1] << ';' << pg->operator[](2)[0] << ',' << pg->operator[](2)[1] << ';'
        << pg->operator[](3)[0] << ',' << pg->operator[](3)[1] << ';' << pg->operator[](4)[0] << ',' << pg->operator[](4)[1] <<
        ';' << pg->operator[](5)[0] << ',' << pg->operator[](5)[1] << endl;

        os << endl;

        // Output the exact shapes.
        for (int i = 0; i < screen.getHeight(); ++i) {
            for (int j = 0; j < screen.getWidth(); ++j) {
                os << screen.getChar(j, i) << ' ';
            }
            os << endl;
        }

        shapeFile.close();
    }

    void eraseAllShapes() {
        shapes.erase(shapes.begin() + 0, shapes.begin() + shapes.size());
    }

    void readFromTextFile() {
        eraseAllShapes();
        filebuf shapeFile;
        shapeFile.open("shapes.txt", ios::in);
        istream is(&shapeFile);

        vector<string> content;
        int lineCounter = 1;

        // Read line-by-line.
        while (!is.eof() && lineCounter < 5) {
            string line;
            is >> line;
            content.push_back(line);
            lineCounter++;
        }

        shapeFile.close();

        // Split each line to get the points' positions.
        for (unsigned long i = 0; i < content.size(); ++i) {
            if (i == 0) {
                vector<program::Point> points;
                splitString(content, points, i);
                shapes.push_back(new program::Rectangle(points.at(0), points.at(1), points.at(2), points.at(3)));
            } else if (i == 1) {
                vector<program::Point> points;
                splitString(content, points, i);
                shapes.push_back(new program::Circle(points.at(0), points.at(1)[0]));
            } else if (i == 2) {
                vector<program::Point> points;
                splitString(content, points, i);
                shapes.push_back(new program::Polyline(points.at(0), points.at(1), points.at(2), points.at(3), points.at(4)));
            } else {
                vector<program::Point> points;
                splitString(content, points, i);
                shapes.push_back(new program::Polygon(points.at(0), points.at(1), points.at(2), points.at(3), points.at(4)));
            }
        }
    }

    void splitString(vector<string> &content, vector<program::Point> &points, unsigned long i) {
        istringstream f(content.at(i));
        string s;
        while (getline(f, s, ';')) {
            string a = s.substr(0, s.find(","));
            string b = s.substr(s.find(",") + 1, s.length());

            program::Point p {stoi(a), stoi(b)};
            points.push_back(p);
        }
    }

    // The svg image will be a scaled version of the shapes printing on the screen.
    void outputToSVGFile(program::Rectangle *r, program::Circle *cr, program::Polyline *pl, program::Polygon *pg) {
        Dimensions dimensions(200, 150);
        Document doc("svg_shapes.svg", Layout(dimensions, Layout::TopLeft));

        doc << (svg::Polygon(Stroke(1, Color::Red)) << svg::Point(r->operator[](1)[0] * 10, r->operator[](1)[1] * 10)
                << svg::Point(r->operator[](2)[0] * 10, r->operator[](2)[1] * 10) <<
                svg::Point(r->operator[](3)[0] * 10, r->operator[](3)[1] * 10) << svg::Point(r->operator[](4)[0] * 10, r->operator[](4)[1] * 10));

        doc << svg::Circle(svg::Point(cr->operator[](0)[0] * 10, cr->operator[](0)[1] * 10), cr->getRadius() * 2 * 10,
                           Fill(Color(255, 255, 255)), Stroke(1, Color::Green));

        doc << (svg::Polyline(Stroke(1, Color::Black)) << svg::Point(pl->operator[](1)[0] * 10, pl->operator[](1)[1] * 10)
                << svg::Point(pl->operator[](2)[0] * 10, pl->operator[](2)[1] * 10) <<
                svg::Point(pl->operator[](3)[0] * 10, pl->operator[](3)[1] * 10) <<
                svg::Point(pl->operator[](4)[0] * 10, pl->operator[](4)[1] * 10) <<
                svg::Point(pl->operator[](5)[0] * 10, pl->operator[](5)[1] * 10));

        doc << (svg::Polygon(Stroke(1, Color::Blue)) << svg::Point(pg->operator[](1)[0] * 10, pg->operator[](1)[1] * 10)
                << svg::Point(pg->operator[](2)[0] * 10, pg->operator[](2)[1] * 10) <<
                svg::Point(pg->operator[](3)[0] * 10, pg->operator[](3)[1] * 10)
                << svg::Point(pg->operator[](4)[0] * 10, pg->operator[](4)[1] * 10) <<
                svg::Point(pg->operator[](5)[0] * 10, pg->operator[](5)[1] * 10));

        doc.save();
    }

    // The png image will be a scaled version of the shapes printing on the screen.
    void outputToPNGFile(Screen &screen) {
        png::image< png::rgb_pixel > image(200, 150);

        for (png::uint_32 y = 0; y < 15; ++y) {
            for (png::uint_32 x = 0; x < 20; ++x) {
                if(screen.getChar(x, y) != ' ') {
                    image[y * 10][x * 10] = png::rgb_pixel(255, 255, 255);
                }
            }
        }

        image.write("png_shapes.png");
    }
};

#endif //WEEK2_APP1_IMAGE_H
