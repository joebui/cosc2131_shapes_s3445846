#ifndef WEEK2_APP1_CIRCLE_H
#define WEEK2_APP1_CIRCLE_H

#include "Line.h"

namespace program {
    class Circle : public Shape {
    private:
        Point center;
        int radius;

    public:
        Circle(Point c, int r): center{c}, radius{r} {}

        // Access the center point of the circle.
        Point operator[] (int x) {
            return center;
        }

        int getRadius() const {
            return radius;
        }

        // Move the rectangle to different location.
        virtual void move(int xx, int yy) {
            center.move(xx, yy);
        }

        // Set and display the shape on the screen.
        virtual void draw(Screen &screen) {
            // Set the north, south, east and west points of circle.
            Point p1 {center[0], center[1] - radius};
            Point p2 {center[0] - radius, center[1]};
            Point p3 {center[0], center[1] + radius};
            Point p4 {center[0] + radius, center[1]};

            Line line1 {p1, p2};
            Line line2 {p2, p3};
            Line line3 {p3, p4};
            Line line4 {p4, p1};

            // Set the symbol for the lines.
            line1.setDraw_char('+');
            line2.setDraw_char('+');
            line3.setDraw_char('+');
            line4.setDraw_char('+');

            line1.draw(screen);
            line2.draw(screen);
            line3.draw(screen);
            line4.draw(screen);
        }
    };
}

#endif //WEEK2_APP1_CIRCLE_H
