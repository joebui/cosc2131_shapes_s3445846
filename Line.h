#ifndef WEEK2_APstart_LINE_H
#define WEEK2_APstart_LINE_H

#include <iostream>
#include "Point.h"

using namespace std;

namespace program {
    class Line : public Shape {
    private:
        Point start;
        Point end;
    public:
        Line(Point pstart, Point pend): start{pstart}, end{pend} {}

        // Access the points of line.
        Point operator[](int x) {
            if (x == 0) {
                return start;
            } else {
                return end;
            }
        }

        // Move the line to a different location.
        virtual void move(int xx, int yy) {
            start.move(xx, yy);
            end.move(xx, yy);
        }

        // Set and display line on the screen.
        virtual void draw(Screen &screen) {
            // Set the char and the start and end point of line.
            screen.setChar(start[0], start[1], getDraw_char());
            screen.setChar(end[0], end[1], getDraw_char());
            Point positionVector {end[0] - start[0], end[1] - start[1]};

            if (start[0] < end[0]) {  // The end point's at the right side of start point.
                // Print all available points within that range.
                for (int i = start[0] + 1; i < end[0]; ++i) {
                    double y = yValue(i, start, positionVector);
                    if (y == (int) y) {
                        screen.setChar(i, (int) y , getDraw_char());
                    }
                }
            } else if (start[0] > end[0]) { // The start point's at the right side of end point.
                // Print all available points within that range.
                for (int i = end[0] + 1; i < start[0]; ++i) {
                    double y = yValue(i, end, positionVector);
                    if (y == (int) y) {
                        screen.setChar(i, (int) y, getDraw_char());
                    }
                }
            } else if (start[0] == end[0]) { // The start and end points are on the same vertical line.
                if (start[1] < end[1]) { // End point is under start point.
                    // Print all available points within that range.
                    for (int i = start[1] + 1; i < end[1]; ++i) {
                        screen.setChar(start[0], i, getDraw_char());
                    }
                } else {
                    // Print all available points within that range.
                    for (int i = end[1] + 1; i < start[1]; ++i) {
                        screen.setChar(end[0], i, getDraw_char());
                    }
                }
            }
        }

        // Calculate y value using line equation.
        double yValue(int x, Point p, Point v) {
            double k = (x - p[0]) / (double) v[0];
            return (v[1] * k + p[1]);
        }

        // Display all properties of Point.
        friend ostream& operator << (ostream& out, Line l) {
            out << l[0] << " - " << l[1] << endl;
            return out;
        }
    };
}

#endif //WEEK2_APstart_LINE_H
