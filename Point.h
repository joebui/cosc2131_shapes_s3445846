#include <iostream>
#include "Shape.h"

#ifndef WEEK2_APP1_POINT_H
#define WEEK2_APP1_POINT_H

using namespace std;

namespace program {
    class Point : public Shape {
    private:
        int x;
        int y;
    public:
        Point(int xx, int yy): x{xx}, y{yy} {}

        // Move the point to a different location.
        virtual void move(int xx, int yy) {
            x += xx;
            y += yy;
        }

        // Set and display point on the screen.
        virtual void draw(Screen &screen) {
            screen.setChar(x, y, getDraw_char());
        }

        // Access the attributes of Point.
        int operator[](int i) {
            if (i == 0) {
                return x;
            } else {
                return y;
            }
        }

        // Display all properties of Point.
        friend ostream& operator<<(ostream& out, Point p) {
            out << "(" << p.x << " , " << p.y << ")";
            return out;
        }
    };
}

#endif //WEEK2_APP1_POINT_H
